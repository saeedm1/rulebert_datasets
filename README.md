# Datasets for rule reasoning

This directory contains the various datasets associated with the paper "RULEBERT: Teaching Soft Rules to Pre-trained Language Models".

The dataset directories are as follows:
  * Chain: The dataset used to for the chaining experiment (Section 6.4). It contains the dataset for depths varying from 0 to 5.
  * Union: The dataset used for the overlapping-rules experiment (Section 6.3).
  * Single_500: A dataset containing examples for 600 single rules and used for training RuleBert_161(Section 6.6).


Each dataset is in the format of JSONL files, named `train.jsonl` (for train split, similar for dev and test).



## Format of Files

### `[train|dev|test].jsonl` files

Here is a sample entry from the  `train.jsonl` file:

```
{'id': 'Chain_0_96588074',



 'context': 'The relation of David is Frank. If the parent of the third person is the first person, and the child of the second person is the third person, and the parent of the third person is the second person, then the spouse of the first person is the second person. The parent of Harry is Anne. The parent of Harry is Gary. The parent of Frank is Gary. The child of Dan is not Grace. The child of Gary is Harry. If there is a relation between the third person and the second person, and there is a relation between the third person and fourth person, and the parent of fourth person is the first person, then the child of the first person is the second person. The spouse of Bob is not Jack. The relation of David is Harry.',


'hypothesis_sentence': 'The child of Gary is not Harry.',


 'output': False,


 'hyp_weight': 0.0,



 'facts_sentence': 'The parent of Harry is Gary. The relation of David is Harry. The spouse of Bob is not Jack. The child of Gary is Harry. The parent of Harry is Anne. The child of Dan is not Grace. The parent of Frank is Gary. The relation of David is Frank.',



 'natural_rule': 'If there is a relation between the third person and the second person, and there is a relation between the third person and fourth person, and the parent of fourth person is the first person, then the child of the first person is the second person. If the parent of the third person is the first person, and the child of the second person is the third person, and the parent of the third person is the second person, then the spouse of the first person is the second person.',




 'facts': 'parent(Harry,Gary). relation(David,Harry). negspouse(Bob,Jack). child(Gary,Harry). parent(Harry,Anne). negchild(Dan,Grace). parent(Frank,Gary). relation(David,Frank).',


 'hypothesis': 'negchild(Gary,Harry)',


 'rule': ['child(A,B) :- relation(C,B),relation(C,D),parent(D,A).',
  'spouse(A,B) :- parent(C,A),child(B,C),parent(C,B).'],


 'rule_support': [0.41, 0.89],



 'solution': {'negchild("Dan","Grace")': '1.0000000000000002',
  'parent("Frank","Gary")': '1.0000000000000002',
  'negspouse("Bob","Jack")': '1.0000000000000002',
  'parent("Harry","Gary")': '1.0000000000000002',
  'child("Anne","Frank")': '0.4099999999999998',
  'spouse("Gary","Anne")': '0.4159167664950491',
  'relation("David","Harry")': '1.0000000000000002',
  'spouse("Anne","Gary")': '0.8900000000000001',
  'child("Gary","Harry")': '1.0000000000000002',
  'child("Gary","Frank")': '0.30379220873078516',
  'spouse("Gary","Gary")': '0.9188462828190753',
  'relation("David","Frank")': '1.0000000000000002',
  'parent("Harry","Anne")': '1.0000000000000002',
  'spouse("Anne","Anne")': '0.4159167664950491',
  'child("Anne","Harry")': '0.4673222095449991'},

 'meta': 'inv_fact',

 }
```

Description of the `train.jsonl` fields: 

* `id`: Example ID
* `context`: Facts and Rule(s) expressed in synthetic language
* `hypothesis_sentence`: The hypothesis to check
* `output`: Binary label indicating whether the hypothesis is valid or not
* `hyp_weight`: The probability of correctness of the hypothesis given by the LPMLN Reasoner
* `facts_sentence`: Facts expressed in synthetic language
* `natural_rule`: Rules expressed unu synthetic language
* `facts`: facts represented as triples
* `hypothesis`: hypothesis represented as a triple
* `rule`: List of rules used in the example
* `rule_support`: Supports of the rules used in the example
* `solution`: Output of the LPMLN Reasoner
* `meta`: How the example was derived, one of (for the chaining datasets, depth is concatenated: **rconc_depth_1**; for the overlapping-rules dataset, the triggered rules are concatenated: **rconc_r1_r2_r3**):
  * "fact" : A fact
  * "switch_fact": A fact with inverted subject and object
  * "inv_fact": A negated fact
  * "rconc": A hypothesis coming from one or more satisfied rules
  * "switch_rconc": A rule conclusion with inverted subject and object
  * "inv_rconc": A negated rule conclusion   
  * "unsat_fact": An unsatisifed fact          
  * "inv_unsat_fact": A negated unsatisfied fact          
  * "unsat_rconc": An unsatisfied rule conclusion          
  * "inv_unsat_rconc": A negated unsatisfied rule conclusion          
        


For some datasets,there are a few additional fields:

* `evidence`: It is found in the chaining datasets with depth greater than zero. It provides a proof graph for the corresponding hypothesis.
* `satisifed_rules`: It is found in the overlapping-rules datasets. It shows which of the rules has been satisfied by the corresponding facts.
* `meta2`: It is found in the overlapping-rules datasets. It is the concatention of two fields: **meta** and **hyp_weight**. It was used for debugging purposes.














